package net.spectre.cordite.entities;


import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ThrowableEntity;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.spectre.cordite.curses.Curse;

public class BulletEntity extends ThrowableEntity{

	private Curse[] curses;
	
	public BulletEntity(World world, LivingEntity liv, Curse... curses) {
		super(CEntities.BULLET, liv, world);
		this.shoot(liv, liv.rotationPitch, liv.rotationYawHead, 0, 3, 0);
		this.curses = curses;
	}
	
	public BulletEntity(World world) {
		this(CEntities.BULLET, world);
	}
	
	public BulletEntity(EntityType<? extends ThrowableEntity> type, World world) {
		super(type, world);
	}

	@Override
	protected void onImpact(RayTraceResult result) {
		
		//Don't kill if non-solid like reeds or wheat or grass
		if(result instanceof BlockRayTraceResult) {
			BlockRayTraceResult block = (BlockRayTraceResult)result;
			if(world.getBlockState(block.getPos()).getCollisionShape(world, block.getPos()).isEmpty())
				return;
		}
		
		
		if(curses != null) {
			for(Curse curse : this.curses) {
				curse.onHit(this, result);
			}
		}
		
		if(result instanceof EntityRayTraceResult) {
			((EntityRayTraceResult)result).getEntity().attackEntityFrom(DamageSource.MAGIC, 10);
			remove();
		}
		
		remove();
	}

	@Override
	public void tick() {
		super.tick();
		if(world.isRemote) {
			world.addParticle(ParticleTypes.SMOKE, posX, posY, posZ, 0, 0, 0);
		}
	}

	@Override
	protected void registerData() {}
	
	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}


}
