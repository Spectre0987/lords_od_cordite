package net.spectre.cordite.entities;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EntityType.Builder;
import net.minecraft.entity.EntityType.IFactory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.spectre.cordite.Cordite;

@Mod.EventBusSubscriber(modid = Cordite.MODID, bus = Bus.MOD)
public class CEntities {
	
	public static EntityType<BulletEntity> BULLET;
	
	@SubscribeEvent
	public static void registry(Register<EntityType<?>> event) {
		event.getRegistry().registerAll(
			BULLET = register("bullet", BulletEntity::new, BulletEntity::new, 0.125F, 0.125F)
		);
	}
	
	public static <T extends Entity> EntityType<T> register(String name, IFactory<T> fact, IEntSpawn<T> client, float width, float height){
		Builder<T> builder = EntityType.Builder.create(fact, EntityClassification.MISC)
			.setShouldReceiveVelocityUpdates(true)
			.setTrackingRange(64)
			.setUpdateInterval(5)
			.setShouldReceiveVelocityUpdates(true)
			.setCustomClientFactory((e, world) -> client.spawn(world))
			.size(width, height);
		
		EntityType<T> type = builder.build(name);
		type.setRegistryName(new ResourceLocation(Cordite.MODID, name));
		return type;
	}
	
	public static interface IEntSpawn<T>{
		T spawn(World world);
	}

}
