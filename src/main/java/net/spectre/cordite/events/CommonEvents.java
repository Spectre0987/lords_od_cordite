package net.spectre.cordite.events;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.spectre.cordite.Cordite;
import net.spectre.cordite.cap.GunCapability;
import net.spectre.cordite.cap.IGun;
import net.spectre.cordite.item.ItemGun;

@Mod.EventBusSubscriber(modid = Cordite.MODID)
public class CommonEvents {

	public static final ResourceLocation GUN = new ResourceLocation(Cordite.MODID, "gun");
	
	@SubscribeEvent
	public static void attachGunCap(AttachCapabilitiesEvent<ItemStack> event) {
		if(event.getObject().getItem() instanceof ItemGun)
			event.addCapability(GUN, new IGun.Provider(new GunCapability(event.getObject(), ((ItemGun)event.getObject().getItem()).getMaxAmmo())));
	}
}
