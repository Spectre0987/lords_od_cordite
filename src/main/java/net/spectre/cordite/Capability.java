package net.spectre.cordite;

import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.fml.common.Mod;
import net.spectre.cordite.cap.IGun;

@Mod.EventBusSubscriber(modid = Cordite.MODID)
public class Capability {

	@CapabilityInject(IGun.class)
	public static final net.minecraftforge.common.capabilities.Capability<IGun> GUN = null;
	
}
