package net.spectre.cordite.net;

import java.util.function.Predicate;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import net.spectre.cordite.Cordite;
import net.spectre.cordite.net.packets.UpdateGun;

public class Network {
	
	private static String VERSION = "1";
	private static Predicate<String> TEST = str -> true;
	
	
	public static SimpleChannel MAIN = NetworkRegistry.newSimpleChannel(new ResourceLocation(Cordite.MODID, "main"), () -> VERSION, TEST, TEST);
	
	public static int ID = 0;
	
	public static void init() {
		
		MAIN.registerMessage(++ID, UpdateGun.class, UpdateGun::encode, UpdateGun::decode, UpdateGun::handle);
		
	}

	public static void sendToPlayer(ServerPlayerEntity player, Object message) {
		MAIN.send(PacketDistributor.PLAYER.with(() -> player), message);
	}

}
