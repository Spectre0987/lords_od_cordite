package net.spectre.cordite.net.packets;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Hand;
import net.minecraftforge.fml.network.NetworkEvent;
import net.spectre.cordite.Capability;

public class UpdateGun {

	CompoundNBT tag;
	Hand hand;
	
	public UpdateGun(Hand hand, CompoundNBT tag) {
		this.tag = tag;
		this.hand = hand;
	}
	
	public static void encode(UpdateGun mes, PacketBuffer buf) {
		buf.writeInt(mes.hand.ordinal());
		buf.writeCompoundTag(mes.tag);
	}
	
	public static UpdateGun decode(PacketBuffer buf) {
		return new UpdateGun(Hand.values()[buf.readInt()], buf.readCompoundTag());
	}
	
	public static void handle(UpdateGun mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			Minecraft.getInstance().player.getHeldItem(mes.hand).getCapability(Capability.GUN).ifPresent(gun -> {
				gun.deserializeNBT(mes.tag);
			});
		});
		context.get().setPacketHandled(true);
	}

}
