package net.spectre.cordite.loot;

import java.util.List;
import java.util.Random;

import com.google.common.collect.Lists;

import net.spectre.cordite.curses.Curse;
import net.spectre.cordite.curses.Curses;

public class GunLootTable {
	
	private static List<GunEntry> CURSES = Lists.newArrayList(
		new GunEntry(10, Curses.ENDER),
		new GunEntry(10, Curses.VENOMOUS),
		new GunEntry(10, Curses.WEAK),
		
		new GunEntry(25, Curses.ILLUMINATING),
		new GunEntry(25, Curses.UNSTABLE),
		new GunEntry(25, Curses.KICK),
		
		new GunEntry(50, Curses.GLOOMSTICK)
	);

	public static Curse getCurse(Random rand) {
		
		for(GunEntry entry : CURSES) {
			if(rand.nextDouble() * 100.0 < entry.weight) {
				//Use this pool
				return entry.curse;
			}
		}
		return Curses.BLADED; //Default
		
	}
	
	public static class GunEntry{
		
		int weight;
		Curse curse;
		
		public GunEntry(int weight, Curse curse) {
			this.weight = weight;
			this.curse = curse;
		}
	}

}
