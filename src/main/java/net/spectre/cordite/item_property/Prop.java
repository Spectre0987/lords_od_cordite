package net.spectre.cordite.item_property;

import java.util.function.Supplier;

import net.minecraft.item.Item.Properties;
import net.minecraft.item.ItemGroup;

public class Prop {

	public static class Item{
		
		public static final Supplier<Properties> GUN = () -> new Properties().maxStackSize(1).group(ItemGroup.COMBAT);
		
	}
}
