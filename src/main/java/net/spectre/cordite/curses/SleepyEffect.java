package net.spectre.cordite.curses;

import net.minecraft.entity.LivingEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.math.RayTraceResult;
import net.spectre.cordite.entities.BulletEntity;

public class SleepyEffect extends Curse{

	public SleepyEffect() {
		
	}
	
	@Override
	public void onHit(BulletEntity entity, RayTraceResult hit) {
		
	}

	@Override
	public void onFire(LivingEntity ent) {
		ent.addPotionEffect(new EffectInstance(Effects.SLOWNESS, 300, 1));
	}

}
