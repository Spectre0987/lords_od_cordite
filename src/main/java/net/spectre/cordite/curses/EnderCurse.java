package net.spectre.cordite.curses;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Items;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.spectre.cordite.entities.BulletEntity;

public class EnderCurse extends Curse {

	public EnderCurse() {
		this.ammo = () -> Items.ENDER_PEARL;
		this.priority = 10;
	}
	
	@Override
	public void onHit(BulletEntity entity, RayTraceResult hit) {
		if(hit instanceof EntityRayTraceResult) {
			EntityRayTraceResult eRay = (EntityRayTraceResult)hit;
			eRay.getEntity().attackEntityFrom(DamageSource.MAGIC, 30);
		}
		
		if(entity.getThrower() != null)
			entity.getThrower().setPositionAndUpdate(entity.posX, entity.posY, entity.posZ);
		
		entity.remove();
	}

	@Override
	public void onFire(LivingEntity ent) {}

	

}
