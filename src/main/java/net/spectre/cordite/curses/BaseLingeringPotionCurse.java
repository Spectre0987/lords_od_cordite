package net.spectre.cordite.curses;

import java.util.function.Supplier;

import net.minecraft.entity.AreaEffectCloudEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Potion;
import net.minecraft.util.math.RayTraceResult;
import net.spectre.cordite.entities.BulletEntity;

public class BaseLingeringPotionCurse extends Curse{

	Effect effect;
	
	public BaseLingeringPotionCurse(Effect effect, Supplier<Item> ammo, int pri) {
		this.effect = effect;
		this.ammo = ammo;
		this.priority = pri;
	}
	
	
	@Override
	public void onHit(BulletEntity entity, RayTraceResult hit) {
		if(!entity.world.isRemote) {
			AreaEffectCloudEntity pot = new AreaEffectCloudEntity(entity.world, entity.posX, entity.posY, entity.posZ);
			pot.setOwner(entity.getThrower());
			pot.setPotion(new Potion(new EffectInstance(effect, 300, 0)));
			pot.setRadius(3F);
			pot.setRadiusOnUse(-0.5F);
			entity.world.addEntity(pot);
		}
	}

	@Override
	public void onFire(LivingEntity ent) {}

}
