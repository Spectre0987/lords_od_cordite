package net.spectre.cordite.curses;

import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.RayTraceResult;
import net.spectre.cordite.entities.BulletEntity;

public class UnstableCurse extends Curse{

	@Override
	public void onHit(BulletEntity entity, RayTraceResult hit) {}

	@Override
	public void onFire(LivingEntity ent) {
		if(!ent.world.isRemote) {
			
			for(int attempts = 0; attempts < 16; ++attempts) {
				int range = 16;
				double posX = ent.posX + (ent.getRNG().nextDouble() - 0.5) * range;
				double posZ = ent.posZ + (ent.getRNG().nextDouble() - 0.5) * range;
				double posY = ent.posY + (ent.getRNG().nextDouble() - 0.5) * range;
				
				if(ent.attemptTeleport(posX, posY, posZ, true))
					break;
			}
			
		}
	}

}
