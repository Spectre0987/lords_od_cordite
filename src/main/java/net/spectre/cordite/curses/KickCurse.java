package net.spectre.cordite.curses;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Items;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.spectre.cordite.entities.BulletEntity;

public class KickCurse extends Curse{

	public KickCurse() {
		this.ammo = () -> Items.IRON_NUGGET;
	}
	
	@Override
	public void onHit(BulletEntity entity, RayTraceResult hit) {}

	@Override
	public void onFire(LivingEntity ent) {
		float ang = (float)Math.toRadians(ent.rotationYawHead);
		ent.setMotion(ent.getMotion().add(Math.sin(ang), 0.5, -Math.cos(ang)));
	}

}
