package net.spectre.cordite.curses;

import net.minecraft.item.Items;
import net.minecraft.potion.Effects;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.spectre.cordite.Cordite;

@Mod.EventBusSubscriber(modid = Cordite.MODID, bus = Bus.MOD)
public class Curses {

	public static IForgeRegistry<Curse> CURSES;
	
	public static final Curse ENDER = new EnderCurse().setRegistryName(new ResourceLocation(Cordite.MODID, "ender"));
	public static final Curse KICK = new KickCurse().setRegistryName(new ResourceLocation(Cordite.MODID, "kick"));
	public static final Curse WEAK = new BaseLingeringPotionCurse(Effects.WEAKNESS, () -> Items.SNOWBALL, 5).setRegistryName(new ResourceLocation(Cordite.MODID, "weak"));
	public static final Curse VENOMOUS = new BaseLingeringPotionCurse(Effects.POISON, () -> Items.SPIDER_EYE, 7).setRegistryName(new ResourceLocation(Cordite.MODID, "venomous"));
	public static final Curse SLEEPY = new SleepyEffect().setRegistryName(new ResourceLocation(Cordite.MODID, "sleepy"));
	public static final Curse GLOOMSTICK = new BaseLingeringPotionCurse(Effects.BLINDNESS, () -> Items.INK_SAC, 3).setRegistryName(new ResourceLocation(Cordite.MODID, "gloomstick"));
	public static final Curse BLADED = new BladedEffect().setRegistryName(new ResourceLocation(Cordite.MODID, "bladed"));
	public static final Curse FROSTY = new FrostyEffect().setRegistryName(new ResourceLocation(Cordite.MODID, "frosty"));
	public static final Curse ILLUMINATING = new IlluminatingCurse().setRegistryName(new ResourceLocation(Cordite.MODID, "illuminating"));
	public static final Curse UNSTABLE = new UnstableCurse().setRegistryName(new ResourceLocation(Cordite.MODID, "unstable"));
	public static final Curse GUNSLINGER = new GunslingerCurse().setRegistryName(new ResourceLocation(Cordite.MODID, "gunslinger"));
	
	@SubscribeEvent
	public static void newRegistry(RegistryEvent.NewRegistry event) {
		CURSES = new RegistryBuilder<Curse>()
				.setType(Curse.class)
				.setName(new ResourceLocation(Cordite.MODID, "curse"))
				.create();
	}
	
	@SubscribeEvent
	public static void onRegistry(Register<Curse> event) {
		event.getRegistry().registerAll(
				ENDER,
				KICK,
				WEAK,
				VENOMOUS,
				SLEEPY,
				GLOOMSTICK,
				FROSTY,
				ILLUMINATING,
				UNSTABLE,
				GUNSLINGER
		);
	}
}
