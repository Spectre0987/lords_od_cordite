package net.spectre.cordite.curses;

import java.util.function.Supplier;

import net.minecraft.block.Blocks;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.registries.IForgeRegistryEntry;
import net.spectre.cordite.cap.IGun;
import net.spectre.cordite.entities.BulletEntity;

public abstract class Curse implements IForgeRegistryEntry<Curse>{

	private ResourceLocation registryName;
	private TranslationTextComponent comp;
	
	protected Supplier<Item> ammo = () -> Blocks.AIR.asItem();
	protected int priority = 0;
	
	@Override
	public Curse setRegistryName(ResourceLocation registryName) {
		this.registryName = registryName;
		this.comp = new TranslationTextComponent("curse." + registryName.getNamespace() + "." + registryName.getPath());
		return this;
	}
	
	@Override
	public ResourceLocation getRegistryName() {
		return this.registryName;
	}
	
	@Override
	public Class<Curse> getRegistryType() {
		return Curse.class;
	}
	
	public abstract void onHit(BulletEntity entity, RayTraceResult hit);
	public abstract void onFire(LivingEntity ent);
	public void onUnselect(PlayerEntity ent) {}
	
	public Item getAmmoType() {
		return this.ammo.get();
	}
	
	public int getPriority() {
		return this.priority;
	}
	
	public TranslationTextComponent getAbjectiveTrans() {
		return comp;
	}
	
	public void findAmmo(IGun gun, PlayerInventory inv) {
		for(ItemStack stack : inv.mainInventory) {
			if(stack.getItem() == ammo.get() && !stack.isEmpty()) {
				gun.setAmmo(gun.getMaxAmmo());
				stack.shrink(1);
			}
		}
	}

	public int getMeleeDamage() {
		return 0;
	}
	
}
