package net.spectre.cordite.curses;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Items;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.spectre.cordite.entities.BulletEntity;

public class IlluminatingCurse extends Curse{

	public IlluminatingCurse() {
		this.ammo = () -> Items.TORCH;
		this.priority = 3;
	}
	@Override
	public void onHit(BulletEntity entity, RayTraceResult hit) {
		
		if(hit instanceof BlockRayTraceResult) {
			BlockRayTraceResult block = (BlockRayTraceResult)hit;
			BlockPos pos = block.getPos().offset(block.getFace());
			if(entity.world.getBlockState(pos).isAir()) {
				
				BlockState state;
				if(entity.world.getBlockState(pos.down()).isSolid())
					state = Blocks.TORCH.getDefaultState();
				else {
					state = Blocks.WALL_TORCH.getDefaultState().with(BlockStateProperties.HORIZONTAL_FACING, block.getFace());
				}
				
				entity.world.setBlockState(pos, state);
			}
		}
		
	}

	@Override
	public void onFire(LivingEntity ent) {}

}
