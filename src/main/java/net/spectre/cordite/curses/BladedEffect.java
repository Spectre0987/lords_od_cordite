package net.spectre.cordite.curses;

import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.RayTraceResult;
import net.spectre.cordite.entities.BulletEntity;

public class BladedEffect extends Curse {

	@Override
	public void onHit(BulletEntity entity, RayTraceResult hit) {}

	@Override
	public void onFire(LivingEntity ent) {}
	
	@Override
	public int getMeleeDamage() {
		return 5;
	}

}
