package net.spectre.cordite.curses;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Items;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.spectre.cordite.entities.BulletEntity;
import net.spectre.cordite.item.ItemGun;

public class GunslingerCurse extends Curse {

	public GunslingerCurse() {
		this.priority = 30;
		this.ammo = () -> Items.APPLE;
	}
	@Override
	public void onHit(BulletEntity entity, RayTraceResult hit) {
		if(hit instanceof EntityRayTraceResult) {
			EntityRayTraceResult ent = (EntityRayTraceResult)hit;
			
			if(ent.getEntity() instanceof LivingEntity) {
				LivingEntity living = (LivingEntity)ent.getEntity();
				
				if(living.getHeldItemMainhand().getItem() instanceof ItemGun || living.getHeldItemOffhand().getItem() instanceof ItemGun)
					living.attackEntityFrom(DamageSource.MAGIC, 10);
			}
			
		}
	}

	@Override
	public void onFire(LivingEntity ent) {}

}
