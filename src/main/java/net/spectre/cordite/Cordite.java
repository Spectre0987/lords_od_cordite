package net.spectre.cordite;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraftforge.fml.DeferredWorkQueue;
import net.minecraftforge.fml.common.Mod;
import net.spectre.cordite.net.Network;

@Mod(Cordite.MODID)
public class Cordite
{
	public static final String MODID = "cordite";
    public static final Logger LOGGER = LogManager.getLogger();
    
    @SuppressWarnings("deprecation")
	public Cordite(){
    	DeferredWorkQueue.runLater(() -> Network.init());
    }

}
