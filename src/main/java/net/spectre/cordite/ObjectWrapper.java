package net.spectre.cordite;

public class ObjectWrapper<T> {

	T obj;
	
	public ObjectWrapper(T obj) {
		this.obj = obj;
	}
	
	public T getObject() {
		return this.obj;
	}
	
	public void setObject(T obj) {
		this.obj = obj;
	}
}
