package net.spectre.cordite.item;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.spectre.cordite.Cordite;
import net.spectre.cordite.item_property.Prop;

@Mod.EventBusSubscriber(modid = Cordite.MODID, bus = Bus.MOD)
public class CItems {

	public static ItemGun DRAGOON;
	public static ItemGun DUET;
	public static ItemGun THUNDERCHILD;
	public static ItemGun ZEUS;
	public static ItemGun MARROW;
	
	@SubscribeEvent
	public static void register(Register<Item> event) {
		event.getRegistry().registerAll(
				DRAGOON = register(new ItemGun(Prop.Item.GUN.get(), 6, 60), "dragoon"),
				DUET = register(new ItemGun(Prop.Item.GUN.get(), 12, 60), "duet"),
				THUNDERCHILD = register(new ItemGun(Prop.Item.GUN.get(), 16, 60), "thunderchild"),
				ZEUS = register(new ItemGun(Prop.Item.GUN.get(), 32, 60), "zeus"),
				MARROW = register(new ItemGun(Prop.Item.GUN.get(), 13, 60), "marrow")
		);
	}
	
	public static <T extends Item> T register(T item, String name) {
		item.setRegistryName(new ResourceLocation(Cordite.MODID, name));
		return item;
	}
	
}
