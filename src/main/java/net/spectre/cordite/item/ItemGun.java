package net.spectre.cordite.item;

import java.util.List;

import com.google.common.collect.Multimap;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.util.LazyOptional;
import net.spectre.cordite.Capability;
import net.spectre.cordite.Cordite;
import net.spectre.cordite.ObjectWrapper;
import net.spectre.cordite.cap.IGun;
import net.spectre.cordite.curses.Curse;

public class ItemGun extends Item{

	public static final String TAG_KEY = Cordite.MODID + ".gun";
	private int maxAmmo = 0;
	private int reloadTime = 0;
	
	public ItemGun(Properties properties, int maxAmmo, int reloadTime) {
		super(properties);
		this.maxAmmo = maxAmmo;
		this.reloadTime = reloadTime;
		this.addPropertyOverride(new ResourceLocation(Cordite.MODID, "gun"),
				(stack, world, player) -> {
					return 0F;
				});
	}
	
	public int getMaxAmmo() {
		return maxAmmo;
	}
	
	public int getReloadTime() {
		return this.reloadTime;
	}
	
	public LazyOptional<IGun> getGun(ItemStack stack){
		return stack.getCapability(Capability.GUN);
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		
		playerIn.setActiveHand(handIn);
		
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
	
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		stack.getCapability(Capability.GUN).ifPresent(gun -> {
			
			tooltip.add(new StringTextComponent("Ammo: " + gun.getAmmo()));
			
			tooltip.add(new StringTextComponent("Reloads with: " + gun.getReloadItem().getName().getFormattedText()));
			
		});
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}
	
	@Override
	public ITextComponent getDisplayName(ItemStack stack) {
		IGun gun = this.getGun(stack).orElse(null);
		if(gun != null) {
			String name = "";
			for(Curse curse : gun.getCurses()) {
				name += curse.getAbjectiveTrans().getFormattedText() + " ";
			}
			return new StringTextComponent(name + super.getDisplayName(stack).getFormattedText());
		}
		return super.getDisplayName(stack);
	}
	
	@Override
	public Multimap<String, AttributeModifier> getAttributeModifiers(EquipmentSlotType equipmentSlot, ItemStack stack) {
		 Multimap<String, AttributeModifier> map = super.getAttributeModifiers(equipmentSlot);
		if(equipmentSlot == EquipmentSlotType.MAINHAND) {
			 stack.getCapability(Capability.GUN).ifPresent(gun -> {
				 
				 int damage = 0;
				 for(Curse curse : gun.getCurses()) {
					 damage += curse.getMeleeDamage();
				 }
				 
				 if(damage > 0)
					 map.put(SharedMonsterAttributes.ATTACK_DAMAGE.getName(), new AttributeModifier("Bladed", damage, AttributeModifier.Operation.ADDITION));
			 });
		}
		 return map;
	}

	@Override
	public ItemStack onItemUseFinish(ItemStack stack, World worldIn, LivingEntity living) {
		if(!worldIn.isRemote && living instanceof ServerPlayerEntity) {
			this.getGun(stack).ifPresent(gun -> {
				gun.reload((PlayerEntity)living);
				gun.update((ServerPlayerEntity)living, living.getActiveHand());
				
			});
		}
		living.stopActiveHand();
		return super.onItemUseFinish(stack, worldIn, living);
	}

	@Override
	public UseAction getUseAction(ItemStack stack) {
		ObjectWrapper<UseAction> action = new ObjectWrapper<UseAction>(UseAction.BLOCK);
		this.getGun(stack).ifPresent(gun -> {
			if(gun.getAmmo() <= 0)
				action.setObject(UseAction.CROSSBOW);
		});
		return action.getObject();
	}

	@Override
	public int getUseDuration(ItemStack stack) {
		IGun gun = this.getGun(stack).orElse(null);
		if(gun != null) {
			if(gun.getAmmo() <= 0)
				return this.getReloadTime() + 1;
			return 72000;
		}
		return 72000;
	}

	@Override
	public void onPlayerStoppedUsing(ItemStack stack, World worldIn, LivingEntity entityLiving, int timeLeft) {
		super.onPlayerStoppedUsing(stack, worldIn, entityLiving, timeLeft);
		
		if(!worldIn.isRemote && entityLiving instanceof ServerPlayerEntity) {
			ServerPlayerEntity player = (ServerPlayerEntity)entityLiving;
			this.getGun(player.getHeldItem(player.getActiveHand())).ifPresent(gun -> {
				gun.fire(player);
			});
		}
	}

	@Override
	public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected);
		if(!worldIn.isRemote) {
			this.getGun(stack).ifPresent(gun -> {
				if(gun.getCurses().length == 0)
					gun.generateEffects();
			});
		}
	}
	
	

}
