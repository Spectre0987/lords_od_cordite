package net.spectre.cordite.cap;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.Constants.NBT;
import net.spectre.cordite.curses.Curse;
import net.spectre.cordite.curses.Curses;
import net.spectre.cordite.entities.BulletEntity;
import net.spectre.cordite.loot.GunLootTable;
import net.spectre.cordite.net.Network;
import net.spectre.cordite.net.packets.UpdateGun;

public class GunCapability implements IGun {

	public static final Random RAND = new Random(System.currentTimeMillis());
	private ItemStack gunStack;
	private int maxAmmo;
	private int ammo;
	private Curse[] curse = {};
	
	public GunCapability(ItemStack stack, int maxAmmo) {
		this.maxAmmo = maxAmmo;
		this.gunStack = stack;
	}

	@Override
	public int getAmmo() {
		return ammo;
	}

	@Override
	public int getMaxAmmo() {
		return this.maxAmmo;
	}

	@Override
	public void setAmmo(int ammo) {
		this.ammo = ammo;
	}
	
	@Override
	public void fire(PlayerEntity entity) {
		if(!entity.world.isRemote) {
			if(this.getAmmo() > 0) {
				BulletEntity bullet = new BulletEntity(entity.world, entity, curse);
				bullet.setPosition(entity.posX, entity.posY + entity.getEyeHeight(), entity.posZ);
				bullet.world.addEntity(bullet);
				
				--this.ammo;
				this.update((ServerPlayerEntity)entity, entity.getActiveHand());
				
				for(Curse curse : this.curse) {
					curse.onFire(entity);
				}
			}
		}
		
	}
	
	@Override
	public void reload(PlayerEntity entity) {
		Curse pri = null;
		for(Curse c : this.curse) {
			if(pri == null || c.getPriority() > pri.getPriority())
				pri = c;
		}
		
		if(pri != null)
			pri.findAmmo(this, entity.inventory);
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("ammo", this.ammo);
		
		ListNBT curseTag = new ListNBT();
		for(Curse loc : this.curse) {
			if(loc != null)
				curseTag.add(new StringNBT(loc.getRegistryName().toString()));
		}
		tag.put("curses", curseTag);
		
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.ammo = tag.getInt("ammo");
		
		ListNBT curseTag = tag.getList("curses", NBT.TAG_STRING);
		List<Curse> newCurses = new ArrayList<Curse>();
		for(INBT nbt : curseTag) {
			Curse c = Curses.CURSES.getValue(new ResourceLocation(((StringNBT)nbt).getString()));
			if(c != null)
				newCurses.add(c);
		}
		this.curse = newCurses.toArray(new Curse[0]);
	}

	@Override
	public Curse[] getCurses() {
		return this.curse;
	}

	@Override
	public void setCurses(Curse... curse) {
		this.curse = curse;
	}
	
	public void update(ServerPlayerEntity player, Hand hand) {
		if(!player.world.isRemote)
			Network.sendToPlayer(player, new UpdateGun(hand, this.serializeNBT()));
	}

	@Override
	public Item getReloadItem() {
		Curse pri = null;
		for(Curse c : this.curse) {
			if(pri == null || c.getPriority() > pri.getPriority())
				pri = c;
		}
		return (pri != null && pri.getAmmoType() != null) ? pri.getAmmoType() : Items.IRON_NUGGET;
	}

	@Override
	public void generateEffects() {
		int count = RAND.nextInt(2) + 2;
		
		this.curse = new Curse[count];
		for(int i = 0; i < count; ++ i) {
			curse[i] = GunLootTable.getCurse(RAND);
		}
		
		this.gunStack.setTag(this.gunStack.getOrCreateTag());
		
	}
}
