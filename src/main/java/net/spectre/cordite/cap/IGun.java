package net.spectre.cordite.cap;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.spectre.cordite.curses.Curse;

public interface IGun extends INBTSerializable<CompoundNBT>{

	int getAmmo();
	int getMaxAmmo();
	
	Curse[] getCurses();
	void setCurses(Curse... curse);
	
	void setAmmo(int ammo);
	
	void fire(PlayerEntity entity);
	void reload(PlayerEntity entity);
	void generateEffects();
	void update(ServerPlayerEntity player, Hand hand);
	
	Item getReloadItem();
	
	public static class Storage implements IStorage<IGun>{

		@Override
		public INBT writeNBT(Capability<IGun> capability, IGun instance, Direction side) {
			return instance.serializeNBT();
		}

		@Override
		public void readNBT(Capability<IGun> capability, IGun instance, Direction side, INBT nbt) {
			instance.deserializeNBT((CompoundNBT)nbt);
		}
		
	}
	
	public static class Provider implements ICapabilitySerializable<CompoundNBT>{

		private IGun gun;
		
		public Provider(IGun gun) {
			this.gun = gun;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
			return cap == net.spectre.cordite.Capability.GUN ? (LazyOptional<T>) LazyOptional.of(() -> gun) : LazyOptional.empty();
		}

		@Override
		public CompoundNBT serializeNBT() {
			return gun.serializeNBT();
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt) {
			gun.deserializeNBT(nbt);
		}
		
	}
}
